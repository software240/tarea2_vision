import cv2 as cv
import numpy as np
import os

# Leer imagen
img = cv.imread(os.path.dirname(__file__) + '\imageA.jpg')

# Multiplicar imagen por una constante
factor = 1.5
img_mult = np.round(np.clip(img * factor, 0, 255)).astype(np.uint8)

# Multiplicar imagen usando la función de OpenCV
img_mult_cv =cv.multiply(img, (factor, factor, factor, factor))

# Mostrar resultados
cv.imshow('Imagen original', img)
cv.imshow('Imagen multiplicada', img_mult)
cv.imshow('Imagen multiplicada con opencv', img_mult_cv)

cv.waitKey(0)
cv.destroyAllWindows()
