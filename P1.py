import cv2 as cv #importando librerias necesarias
import sys
import os

img_original = cv.imread(os.path.dirname(__file__) + "\ImageA.jpg") #se carga la imagen

if img_original is None: #verifica si se cargo bien la imagen, en caso contrario genera un mensaje de error
    sys.exit("Could not read the image.")

num_fil = img_original.shape[0] #obtiene el numero de filas en la imagen
num_col = img_original.shape[1] #obtiene el numero de columnas en la imagen

# Pregunta a usuario vértices de la región a recortar
f1 = int(input("Ingrese la fila del vértice superior izquierdo: "))
c1 = int(input("Ingrese la columna del vértice superior izquierdo: "))
f2 = int(input("Ingrese la fila del vértice inferior derecho: "))
c2 = int(input("Ingrese la columna del vértice inferior derecho: "))

# Comprueba que los datos hayan sido ingresados correctamente
if f1 < 0: f1 = 0
if f1 > num_fil: f1 = num_fil

if f1 < f2 and c1 < c2:
	img = img_original[f1:f2,c1:c2,:]
else:
	sys.exit("error en las filas o columnas ingresadas")

cv.imshow("Imagen Recortada", img)#se muestra la imagen Recortada
k=cv.waitKey(0) #cierra las ventanas de imagen con una tecla cualquiera