import cv2 as cv
import numpy as np
import os

# Se pregunta al usuario por el factor del zoom
factor = int(input("Ingrese el factor del zoom a aplicar en la imagen:"))

# Se crea matriz de puntos para obtener coordenadas del clic del mouse en la imagen
point_matrix = np.zeros((2,2),int)

# se inicia el contador de clics en cero
counter = 0

# se define una funcion que regitra los puntos donde se hace clic y la cantidad de clics hechos
def mousePoints(event,x,y,flags,params):
    global counter
    # Evento de clic del botón izquierdo del mouse
    if event == cv.EVENT_LBUTTONDOWN:
        point_matrix[counter] = x,y
        counter = counter + 1

# Lee la imagen y hace una copia
img_original = cv.imread(os.path.dirname(__file__) + "\ImageA.jpg")
img_original_copia  = img_original.copy()

# Crea ventana para mostrar la imagen
cv.namedWindow("Original Image ")

# bucle while necesario para registrar los clics en la imagen y posteriormente recortarla
while True:
    for x in range (0, counter):
        cv.circle(img_original,(point_matrix[x][0],point_matrix[x][1]),3,(0,255,0),cv.FILLED)

    # si se cuentan 2 clics, se guardan las coordenadas de los clics
    if counter == 2:
        starting_x = point_matrix[0][0]
        starting_y = point_matrix[0][1]

        ending_x = point_matrix[1][0]
        ending_y = point_matrix[1][1]
        # Dibuja un rectángulo para el área de interés
        cv.rectangle(img_original, (starting_x, starting_y), (ending_x, ending_y), (0, 255, 0), 3)
        cv.imshow("Original Image ", img_original)

    # Evento de clic del mouse en la imagen original
    cv.setMouseCallback("Original Image ", mousePoints)
    # Se define una variable "key"
    key = cv.waitKey(1)
    # Si se presiona "q" se rompe el bucle (y el resto de codigo se invalida en consecuencia)
    if key == ord('q'):
        break
    #si se presiona "Espacio" se recorta la imagen y se continua ejecutando el resto de codigo
    elif key == ord(' '):
        if counter == 2:
            # genera el recorte de la imagen
            img = img_original_copia[starting_y:ending_y, starting_x:ending_x]
            cv.imshow("ROI", img)
            # Sale del bucle principal
            break

    # muestra la imagen original y la mantiene en primera prioridad
    cv.imshow("Original Image ", img_original)
    cv.setWindowProperty("Original Image ", cv.WND_PROP_TOPMOST, 1)

#se almacena la imagen recortada como variable global
img = img_original_copia[starting_y:ending_y, starting_x:ending_x]
# se crea una copa del recorte
img_copy = img.copy()

# se itera en funcion del factor de zoom ingresado ateriormente
for x in range(0,factor,1):
    filas = img.shape[0] #obtiene el numero de filas en la imagen recortada
    columnas = img.shape[1] #obtiene el numero de columnas en la imagen recortada

    # se define la matriz de ceros extendida
    imagen_extendida = np.zeros((2*filas+1, 2*columnas+1, 3), np.uint8)

    # se copia los valores de la imagen original en la matriz extendida
    imagen_extendida[1:-1:2, 1:-1:2,:] = img

    # Se define la máscara de convolución
    MC = np.array([[1/4, 1/2, 1/4],
                [1/2, 1, 1/2],
                [1/4, 1/2, 1/4]])

    # Se aplica la máscara de convolución a la imagen extendida
    imagen_convolucionada = np.zeros((imagen_extendida.shape[0]-2, imagen_extendida.shape[1]-2, 3), np.uint8)
    for i in range(1, imagen_extendida.shape[0]-1):
        for j in range(1, imagen_extendida.shape[1]-1):
            for k in range(3): #se itera sobre todas las bandas de color
                imagen_convolucionada[i-1,j-1,k] = np.sum(MC * imagen_extendida[i-1:i+2, j-1:j+2, k])
    img = imagen_convolucionada.copy()

#se muestra el zoom de la imagen mediante mascara de convolucion
cv.imshow("Zoom Imagen mascara de convolucion", imagen_convolucionada)

# se define la funcion zoom nativa de open cv
def zoom(img, zoom_factor):
    return cv.resize(img, None, fx=zoom_factor, fy=zoom_factor)

img_zoom_cv = zoom(img_copy, factor+1)

# se muestra el zoom de open cv
cv.imshow("Imagen zoom cv", img_zoom_cv )
k=cv.waitKey(0)
