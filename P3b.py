import cv2 as cv
import numpy as np
import os

# Leer imágenes
img1 = cv.imread(os.path.dirname(__file__) + '\imageA.jpg')
img2 = cv.imread(os.path.dirname(__file__) + '\imageB.jpg')

# Verificar que las dimensiones de las imágenes sean iguales
if img1.shape != img2.shape:
    print("Error, las dimensiones de las imágenes no son iguales")
    exit()


num_fil = img1.shape[0]
num_col = img2.shape[1]

# Restar imágenes usando Python
#img_diff = img1 - img2
imgDiff = np.zeros((num_fil,num_col,3), np.uint8)

for fil in range(num_fil):
    for col in range(num_col):
        if int(img1[fil,col,0]) - int(img2[fil,col,0]) >= 0:
            imgDiff[fil,col,0] = img1[fil,col,0] - img2[fil,col,0]
        else:
            imgDiff[fil,col,0] = 0
        if int(img1[fil,col,1]) - int(img2[fil,col,1]) >= 0:
            imgDiff[fil,col,1] = img1[fil,col,1] - img2[fil,col,1]
        else:
            imgDiff[fil,col,1] = 0
        if int(img1[fil,col,2]) - int(img2[fil,col,2]) >= 0:
            imgDiff[fil,col,2] = img1[fil,col,2] - img2[fil,col,2]
        else:
            imgDiff[fil,col,2] = 0

# Restar imágenes usando la función subtract de OpenCV
img_diff_cv = cv.subtract(img1, img2)

# Mostrar resultados
cv.imshow('Imagen 1', img1)
cv.imshow('Imagen 2', img2)
cv.imshow('Resta', imgDiff)
cv.imshow('Resta con cv2.subtract()', img_diff_cv)

cv.waitKey(0)
cv.destroyAllWindows()
