import cv2 as cv
import numpy as np
import os

# leer la imagen
img = cv.imread(os.path.dirname(__file__) + '\ImageA.jpg')

# Divide la imagen por una constante
factor = 1.5
imgDivided = np.round(np.divide(img, factor)).astype(np.uint8)

# Divide la imagen utilizando la funcion de opencv
imgDividedCV = cv.divide(img, (factor, factor, factor, factor))

# Show original image and the divided images
cv.imshow("Original", img)
cv.imshow("Dividida por una constante", imgDivided)
cv.imshow("Dividida utilizando OpenCV", imgDividedCV)

cv.waitKey(0)
