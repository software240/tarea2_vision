import cv2 as cv
import sys
import numpy as np
import os

#Definicion de Filtro "Sal y pimienta para aplicar a la foto"
def noisy(noise_typ,image):
   if noise_typ == "gauss":
      row,col,ch= image.shape
      mean = 0
      var = 0.1
      sigma = var**0.5
      gauss = np.random.normal(mean,sigma,(row,col,ch))
      gauss = gauss.reshape(row,col,ch)
      noisy = image + gauss
      return noisy
   elif noise_typ == "s&p":
      row,col,ch = image.shape
      s_vs_p = 0.5
      amount = 0.004
      out = np.copy(image)
      # Salt mode
      num_salt = np.ceil(amount * image.size * s_vs_p)
      coords = [np.random.randint(0, i - 1, int(num_salt))
              for i in image.shape]
      out[tuple(coords)] = 1

      # Pepper mode
      num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
      coords = [np.random.randint(0, i - 1, int(num_pepper))
              for i in image.shape]
      out[tuple(coords)] = 0
      return out
   elif noise_typ == "poisson":
      vals = len(np.unique(image))
      vals = 2 ** np.ceil(np.log2(vals))
      noisy = np.random.poisson(image * vals) / float(vals)
      return noisy
   elif noise_typ =="speckle":
      row,col,ch = image.shape
      gauss = np.random.randn(row,col,ch)
      gauss = gauss.reshape(row,col,ch)        
      noisy = image + image * gauss
      return noisy
   
def filtro_promedio(image, kernel_size):
    # Función para aplicar el filtro promedio a la imagen
    kernel = np.ones((kernel_size, kernel_size), np.float32) / (kernel_size**2)
    img_prom = cv.filter2D(image, -1, kernel)
    return img_prom


def filtro_medio(image, kernel_size):
    # Función para aplicar el filtro mediano a la imagen
    img_fmedio = cv.medianBlur(image, kernel_size)
    return img_fmedio

#Cargar la imagen
img = cv.imread(os.path.dirname(__file__) + '\papagayo.jpg')

if img is None:
    sys.exit("Could not read the image.")

num_fil = img.shape[0]
num_col = img.shape[1]


imgRuido = noisy('s&p',img)
imgFiltromedio  = filtro_medio(imgRuido,3)
imgFiltroprom = filtro_promedio(imgRuido,3)
imgFiltroCV = imgRuido.copy()

# Aplicar la función medianBlur de OpenCV
for fil in range(1, num_fil - 1):
    for col in range(1, num_col - 1):

       imgFiltroCV = cv.medianBlur(imgFiltroCV, 3) 

# Mostrar las imágenes
cv.imshow("Imagen ruido", imgRuido)
cv.imshow("Imagen fmedio", imgFiltromedio)
cv.imshow("Imagen fpromedio", imgFiltroprom)
cv.imshow("Imagen filtrada OpenCV", imgFiltroCV)
cv.waitKey(0)
    


