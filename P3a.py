import cv2 as cv
import numpy as np
import os

# Leer imágenes
img1 = cv.imread(os.path.dirname(__file__) + '\imageA.jpg')
img2 = cv.imread(os.path.dirname(__file__) + '\imageB.jpg')

# Verificar que las dimensiones de las imágenes sean iguales
if img1.shape != img2.shape:
    print("Error, las dimensiones de las imágenes no son iguales")
    exit()

num_fil = img1.shape[0]
num_col = img2.shape[1]

# Sumar imágenes pixel a pixel

imgSum = np.zeros((num_fil,num_col,3), np.uint8)

for fil in range(num_fil):
    for col in range(num_col):
        if int(img1[fil,col,0]) + int(img2[fil,col,0]) <= 255: imgSum[fil,col,0] = img1[fil,col,0] + img2[fil,col,0]
        else: imgSum[fil,col,0] = 255
        if int(img1[fil,col,1]) + int(img2[fil,col,1]) <= 255: imgSum[fil,col,1] = img1[fil,col,1] + img2[fil,col,1]
        else: imgSum[fil,col,1] = 255
        if int(img1[fil,col,2]) + int(img2[fil,col,2]) <= 255: imgSum[fil,col,2] = img1[fil,col,2] + img2[fil,col,2]
        else: imgSum[fil,col,2] = 255

# Sumar imágenes usando la función add de OpenCV
img_sum_cv = cv.add(img1, img2)

# Sumar imágenes usando la función addWeighted de OpenCV
img_sum_cv_weighted = cv.addWeighted(img1, 0.5, img2, 0.5, 0)

# Mostrar resultados
cv.imshow('Imagen 1', img1)
cv.imshow('Imagen 2', img2)
cv.imshow('Suma', imgSum)
cv.imshow('Suma con cv2.add()', img_sum_cv)
cv.imshow('Suma ponderada con cv2.addWeighted()', img_sum_cv_weighted)

cv.waitKey(0)
cv.destroyAllWindows()
